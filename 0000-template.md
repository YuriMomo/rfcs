RFC: Combat and Progression
===========================

# Problem

A core feature missing for Veloren is a player stats/progression and combat system. Many discussion were led onto this topic but none of the ideas many people agreed on were written down. This RFC tries to sum those up and complete missing parts with proven concepts from other similar games.


# Current State

The current state as of the 1st of August, 2019 is a very basic combat system that lets the player damage opponents by hitting them pressing the left mouse button. Lowering their hitpoints to 0 grants him one experience point and will eventually raise his level when meeting the experience requirement. 


# Proposer

Pfauenauge

# Proposal

Goals

The aim of the following suggestions is to create an easy to start with but hard to master combat system. It’s supposed to not scare off new users by being over complicated or requiring a certain amount of skill to get into it.
On the other hand it should be complex enough to necessitate a certain amount of practise to really squeeze out the maximum from your character and playstyle.

Another important goal is to make the combat feel responsive and action oriented instead of having waiting times or suffering from constant stationary battles.

How can that be achieved?

Introduction

The first few moments in a newly started game decide whether the player will continue to play it or not. Combat will be a big factor in this first impression.

We can expect at least two major groups of players: The total novices and the ones that are already familiar with action RPGs.

To pick up the beginner group we will need an easy to understand system without throwing a massive amount of numbers and rules at the player. Recent games tend to overly hold the hands of those people. We might want to avoid that to increase the feeling of having achieved something by yourself.


The more experienced group wants to feel “at home” while still being offered something new and exciting. Therefore we also need to adapt to the general rules of RPG designs without completely reinventing the wheel just for the sake of it.

The system suggested will be a colourful feature mix from many other games that have proven to work and excite gamers.

So, let’s jump right in.


Level Up!

Disclaimer: All showed UI-parts are examples for illustrating functionality and no planned ingame elements.

Congratulations! Ding! You just gained your first level!

So.. what’s happening now?

Suddenly the character window button starts flashing. You click it.

![](https://cdn.discordapp.com/attachments/484371309455736842/606647133399154698/unknown.png)


You are greeted with a simple window, showing 4 values with slightly flashing “+” signs beneath them.

Hovering their names will give you this information:

Constitution: Determines your base hitpoints and stamina. (stamina will be explained later)

Strength: Determines the base damage you deal with melee and non-magical distance weapons (aka bows).

Dexterity: Determines your chance to land critical attacks with melee, distance and magical weapons and attacks.

Intelligence: Determines your base damage with magical weapon attacks and base mana value.


Having chosen the big axe (and an even bigger orc) at the char selection you obviously want to become a mighty axe wielding terror of the lands.
So your spend your points like this:

![](https://cdn.discordapp.com/attachments/484371309455736842/606647178986913859/unknown.png)


After spending those points you are presented with the message that a “Skillpoint is available.”

Clicking the flashing arrow will open the spellbook.

![](https://cdn.discordapp.com/attachments/484371309455736842/606647251321880716/unknown.png)

This is one of the Skill trees.

Red is for strength oriented skills.

It will give you better combos (explained later) for melee weapons.
Additional skills you can use with those melee weapons without having any particular type equipped.
Inside this tree there are also passive skills that will be useful when choosing this style of fighting. (E.g. raising your damage with them or granting additional effects)


Yellow represents the dexterity skill tree.

It will be all about fighting with daggers, dual wielded weapons and bows.


The blue intelligence tree will be about casting magical damage and healing spells.


The player will be able to invest a certain amount of points per level into those skills/spells/passives inside those trees.


But there is more!

Having spent a certain amount of points into one, two or all three of those skills trees will grant you a class in form of a name and boni fitting what you chose.

You can choose to just follow one path and spend most of your points into one of the trees.
This, in case of the strength tree, will make you able to raise from a simple “Fighter” to a “Warrior” and more titles to the last rank of a “Warlord”. (Names are still to be discussed…)


Like mentioned above those ranks will also grant you (fitting) stat boni to give you another sensation of reward and a reason to stay on your chosen route.

Something like investing into every tree would be totally possible and would lead to the player being able to use every weapon and fighting style available in a medium but still effective way. 



```
Examples:

Strength Titles

Title 1, 
Requires: 10 str points spent
Awards: +100HP +10 Melee Weapon Damage
```


Title 2: Requires more points in str, awards more useful features for this playstyle


> Strength, Dex Title
> 
> Title 1
> Requires: 5 Str, 5 Points Dex: 
> Awards: +10 Weapon Damage, +0,5% Crit Chance.



Strength, Dex, Int Title

Title 1
Requires: 3 Str, 3 Dex, 3 Int
Awards:  +5 Weapon Damage, +0,25% Crit Chance, + 30 Mana


Actual Fighting

So how are those skills actually being used?

The base principle is easy.

Left Clicking will initiate a single basic attack with your mainhand weapon. Depending on the weapon this will be either a melee hit, firing an arrow or launching a magic missile.
The following left click will either result in a hit with your offhand weapon, or the next mainhand hit.

Holding the left mouse button charges the attack, increases the hit counter by X(explained later) and deals X times more damage than an uncharged attack.

Right Click/holding: This will bring you into a parrying/blocking state.
Staying in this state for too long will make you dizzy and stunned for 1 second.

Combos

Spending points into certain weapon types in the skill trees will unlock (more) combos for them. 
How do those work?
Hitting your target with simple left click attacks will activate a hit counter. If you managed to hit your opponent X-times the next attack will be a stronger attack with a different animation/side effect.
Attacks being blocked, dodged/avoided or missing will lower that counter by X.
Not attacking the enemy at all for more than X seconds will lower the value to 0.

What do I need the extra skills and spells from the skill tree for?

Those are used to react to certain situations. 

Examples:

A) Your opponent tries to run away in order to break your hit counter. You skilled “Weapon chain”. This skill pulls the enemy back to you and slows him for X seconds.

B) Your opponent just pulled you to him but you want to avoid his melee attacks at all costs. So you cast a teleportation skill to create some distance that you can use to activate a damage shield or to sling another magical attack into his direction.

C) Your opponent just teleported himself away and starts raising his hitcounter by throwing fire balls at you. You really need to get to him before he can unleash a combo attack. You sacrifice your last energy resources to storm into his direction at full speed.

D) You see your friend being charged by a warrior. You aim one of your heal spells at him. Being at full health again he can easily kill the wounded warrior.


Talking about…

Energy ressources

There are three energy ressources (does this number sound familiar?)

Mana, the grandfather of all rpg energy ressources, will fuel every magical spell, buff, or debuff. It has a relatively high maximum value but a very low regeneration rate.

Rage will be used for strength based attacks. It starts at zero but will rise with every hit the player deals and every hit the player takes. It will be used for attacks with two handed weapons and dual wielded weapons. It’s very effective when you plan to fight in the first row and get hit a lot and regenerates faster than focus under those circumstances.

Focus will be used for dex based attacks with daggers, dual wielded weapons and bows.
It has a relatively low maximum value (that can’t be raised by stats) but a very high (infight) regeneration. It will grant a constant damage output without being hit.


But can’t I mix and match the skill trees? Will I have to manage three energy ressources?!

No.

Every skill has it’s relative energy consumption in all three ressources. 
It’s up to you to choose the most viable ressource for your playing style. 

A spell/skill will always be the cheapest when used with the energy ressource of it’s own “school”


For example a healspell will cost a large amount of rage/focus but only a small portion of mana. Pulling your enemy with “Weapon Chain” will cost X% of your chosen ressource, no matter if it’s mana, rage or focus.


Blocking

The player can block attacks with every weapon. Even bows and staffs.

When raising your weapon to block an attack a counter starts.

The longer you block the lower the chance to actually block an attack. When the counter reaches zero you get exhausted and can’t attack for 2 full seconds. (Global cooldown on every attack.)

Having fully blocked an attack will lead to the opponent for not being able to attack or cast for 2 seconds (Global cooldown on every attack.) 

Magic and arrows can only be blocked by a shield.

Magical shields can only block a certain amount of damage until they stop existing.


This mechanic is supposed to be used against charged attacks you can’t avoid. For example by bosses you can’t run away from.


Stamina

This value determines your endurance while performing physical activities.
Those are climbing and hanggliding. (Maybe even some form of sprinting)

I would strongly advise against using stamina as a value specifying the amount of hits you can deal with your weapon. The player has to keep an eye on his health and energy ressource plus his skills when fighting. A third value would overcomplicate the situation without much advantage to it.


Skilling your weapons.

You can’t just take a sword and swing it like a master on day one.

Actively using it will raise your experience level with this kind of weapon.

Skilling combos will not only need the right amount of skill points from your general experience but also a certain training level with the specific weapon that combo is used with.

Example:

[2h Sword Combo No.2]: (In the skill tree)
Requires :
Level 20
Level 15 2h Swords.


Attacking an (NPC!) enemy with an unskilled weapon will lead to significantly less damage inflicted.
Attacking it with a weapon training level way above it’s general level will lead to much higher damage values.


























